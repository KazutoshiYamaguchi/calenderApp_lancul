//
//  sendDBModel.swift
//  calenderApp_lancul
//
//  Created by 山口千寿 on 2021/12/08.
//

import Foundation
import FirebaseStorage
import UIKit
import FirebaseFirestore

struct SendDBModel{
  
  var db=Firestore.firestore()
  
  var yearString=String()
  var monthString=String()
  var dayString=String()
  var locationString=String()
  var usernameString=String()
  var startingTimeString=String()
  var endingTimeString=String()
  
  
  //送信機能を集約
  //incorporate functions to send
  init(){
    
    
  }
  
  init(yearString:String,monthString:String,dayString:String,locationString:String,usernameString:String,startingTimeString:String,endingTimeString:String){
    
    //add "self" at the beginning to differentiate the argument you've just fetched from the valuable in the current class
    self.yearString=yearString
    self.monthString=monthString
    self.dayString=dayString
    self.locationString=locationString
    self.usernameString=usernameString
    self.startingTimeString=startingTimeString
    self.endingTimeString=endingTimeString
  
  }
  
  func sendStringData(){
    
    db.collection(yearString).document(monthString).collection(dayString).document(locationString).collection(usernameString).document().setData(["Starting Time" : self.startingTimeString,"EndingTime":self.endingTimeString])
    
  
  }
  
  
}
