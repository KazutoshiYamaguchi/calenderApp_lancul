//
//  ReturnWeekday.swift
//  calenderApp_lancul
//
//  Created by 山口千寿 on 2021/12/07.
//

import Foundation

class ReturnWeekday{

  func returnWeekdayString(weekday:Int)->String{
    
    
    if weekday==1{
      return "Sun"
    }
    else if weekday==2{
      return "Mon"
    }
    else if weekday==3{
      return "Tue"
    }
    else if weekday==4{
      return "Wed"
    }
    else if weekday==5{
      return "Thu"
    }
    else if weekday==6{
      return "Fri"
    }
    else if weekday==7{
      return "Sat"
    }
 
    return ""
  }

}
