//
//  ViewController.swift
//  calenderApp_lancul
//
//  Created by 山口千寿 on 2021/12/02.
//

import UIKit
import FSCalendar
import CalculateCalendarLogic


class ViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate,FSCalendarDelegateAppearance, UIGestureRecognizerDelegate, UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource {
  

  var returnweekday=ReturnWeekday()
  let preferedLocationArr = ["Tokyo","Yokohama","Ikebukuro","Shinzyuku","Shibuya"]
  
  var usernameString="Kazu"
  var yearString=String()
  var monthString=String()
  var dayString=String()
  var startingTimeString=String()
  var endingTimeString=String()
  var locationString=String()
  
 
  @IBOutlet weak var calendar: FSCalendar!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var startingTimePicker: UIDatePicker!
  @IBOutlet weak var endingTimePicker: UIDatePicker!
  @IBOutlet weak var preferedLocationPicker: UIPickerView!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    calendar!.delegate=self
    calendar.dataSource=self
    preferedLocationPicker.delegate=self
    preferedLocationPicker.dataSource=self
  
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    preferedLocationPicker.selectRow(2, inComponent: 0, animated: true)
    setCurrentDate()
    resetDatePicker()
    convertDateDataToString()
    locationString = preferedLocationArr[2]
    
    print(startingTimeString)
    print(endingTimeString)
    print(yearString)
    print(monthString)
    print(dayString)
    print(locationString)
  }
  
  func setCurrentDate(){
    
    let date = Date()
    let formatter = DateFormatter()
 
    let tmpDate = Calendar(identifier: .gregorian)
    let year = tmpDate.component(.year, from: date)
    let month = tmpDate.component(.month, from: date)
    let day = tmpDate.component(.day, from: date)
    let weekday = tmpDate.component(.weekday, from: date)
    let weekdayString=returnweekday.returnWeekdayString(weekday: weekday)
 
    let monthSym=formatter.shortMonthSymbols[month-1]
    
    //store each valuable in each
    yearString=String(year)
    monthString=String(monthSym)
    dayString=String(day)
    
    dateLabel.text = "\(month) / \(day) (\(weekdayString)) "
  
  }
  
  func resetDatePicker(){
    
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    startingTimePicker.date = formatter.date(from: "12:00")!
    endingTimePicker.date = formatter.date(from: "12:00")!
  }
  
  
  func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition){
   
    setDateLabel(date: date)
    //resetDatePicker()
  }
  
  func setDateLabel(date:Date){
    let tmpDate = Calendar(identifier: .gregorian)
    let year = tmpDate.component(.year, from: date)
    let month = tmpDate.component(.month, from: date)
    let day = tmpDate.component(.day, from: date)
    let weekday = tmpDate.component(.weekday, from: date)
    let weekdayString=returnweekday.returnWeekdayString(weekday: weekday)
    
    let formatter = DateFormatter()
    let monthSym=formatter.shortMonthSymbols[month-1]
    
    
    //store each valuable in each
    yearString=String(year)
    monthString=String(monthSym)
    dayString=String(day)
    
    dateLabel.text = "\(month) / \(day) (\(weekdayString)) "
  }
  

  
  @IBAction func done(_ sender: Any) {
    
    convertDateDataToString()
    
    //store this in database
    print(startingTimeString)
    print(endingTimeString)
    print(yearString)
    print(monthString)
    print(dayString)
    print(locationString)
    
    showAlert()

  }
  
  func convertDateDataToString(){
    
    let formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    startingTimeString=formatter.string(from: startingTimePicker.date)
    endingTimeString=formatter.string(from: endingTimePicker.date)
    
  }
  
  func showAlert(){
    
    let alert = UIAlertController(title: "Are you sure you want to submit this shift?", message: "\(monthString) \(dayString), \(yearString) \(startingTimeString) ~ \(endingTimeString)\nLocation:\(locationString)", preferredStyle: .alert)
    let yes = UIAlertAction(title: "Yes", style: .default) { (action) in
      
      //when "Yes" is pressed, data is sent to database
      self.sendData()

        self.dismiss(animated: true, completion: nil)
    }
    
    let no = UIAlertAction(title: "No", style: .cancel) { (acrion) in
        self.dismiss(animated: true, completion: nil)
    }
    
    alert.addAction(yes)
    alert.addAction(no)
    present(alert, animated: true, completion: nil)
  }
  
  
  
  func sendData(){
    
    let sendDBModel=SendDBModel(yearString: yearString, monthString:monthString, dayString: dayString, locationString: locationString, usernameString: usernameString, startingTimeString: startingTimeString, endingTimeString: endingTimeString)
    
    sendDBModel.sendStringData()
    
  }
  
  /**
   set picker view of the prefered locations
   */
  
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return preferedLocationArr.count
  }
  
  //表示内容
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return preferedLocationArr[row]
     }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
         //store prefered location in locationString
          locationString = preferedLocationArr[row]
      }
  
  
  
  
  /**
   change the colors of holidays (ignore this below)
   */
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  

  fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
  fileprivate lazy var dateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd"
      return formatter
  }()


    // 祝日判定を行い結果を返すメソッド(True:祝日)
    func judgeHoliday(_ date : Date) -> Bool {
        //祝日判定用のカレンダークラスのインスタンス
        let tmpCalendar = Calendar(identifier: .gregorian)

        // 祝日判定を行う日にちの年、月、日を取得
        let year = tmpCalendar.component(.year, from: date)
        let month = tmpCalendar.component(.month, from: date)
        let day = tmpCalendar.component(.day, from: date)

        // CalculateCalendarLogic()：祝日判定のインスタンスの生成
        let holiday = CalculateCalendarLogic()

        return holiday.judgeJapaneseHoliday(year: year, month: month, day: day)
    }
    // date型 -> 年月日をIntで取得
    func getDay(_ date:Date) -> (Int,Int,Int){
        let tmpCalendar = Calendar(identifier: .gregorian)
        let year = tmpCalendar.component(.year, from: date)
        let month = tmpCalendar.component(.month, from: date)
        let day = tmpCalendar.component(.day, from: date)
        return (year,month,day)
    }

    //曜日判定(日曜日:1 〜 土曜日:7)
    func getWeekIdx(_ date: Date) -> Int{
        let tmpCalendar = Calendar(identifier: .gregorian)
        return tmpCalendar.component(.weekday, from: date)
    }

    // 土日や祝日の日の文字色を変える
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        //祝日判定をする（祝日は赤色で表示する）
        if judgeHoliday(date){
            return UIColor.red
        }

        //土日の判定を行う（土曜日は青色、日曜日は赤色で表示する）
        let weekday = getWeekIdx(date)
        if weekday == 1 {   //日曜日
            return UIColor.red
        }
        else if weekday == 7 {  //土曜日
            return UIColor.blue
        }

        return nil
    }

}



